import pubmed
import zotero

if __name__ == "__main__":
    search_result = pubmed.search()
    zotero.update_result(search_result)
