import configparser
import pubmed
from pyzotero import zotero

config = configparser.ConfigParser()
config.read('config.ini')
API_KEY = config['zotero']['api_key']
LIBRARY_ID = config['zotero']['library_id']
LIBRARY_TYPE = config['zotero']['library_type']

zot = zotero.Zotero(library_id=LIBRARY_ID, library_type=LIBRARY_TYPE, api_key=API_KEY)
first_ten = zot.top(limit=10)
print(first_ten)

