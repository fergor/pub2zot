import configparser
from dataclasses import dataclass
from typing import List
from datetime import datetime

from Bio import Entrez

@dataclass
class Article:
    id: str
    pubdate: datetime
    title: str
    authors: List[str]
    journal_short: str
    journal_full: str
    title: str
    volume: str
    issue: str
    pages: str
    lang: str
    issn: str
    doi: str
    pubtype: str
    def __init__(self, **kwargs):
        self.id = kwargs['Id']
        self.pubdate = kwargs['PubDate']
        self.journal = kwargs['Source']
        self.authors = kwargs['AuthorList']

    def zotero_format(self):
        {u'ISBN': u'0810116820',
         u'abstractNote': u'',
         u'accessDate': u'',
         u'archive': u'',
         u'archiveLocation': u'',
         u'callNumber': u'HIB 828.912 BEC:3g N9',
         u'collections': [u'2UNGXMU9'],
         u'creators': [{u'creatorType': u'author',
                        u'firstName': u'Daniel',
                        u'lastName': u'Katz'}],
         u'date': u'1999',
         u'dateAdded': u'2010-01-04T14:50:40Z',
         u'dateModified': u'2014-08-06T11:28:41Z',
         u'edition': u'',
         u'extra': u'',
         u'itemType': u'book',
         u'key': u'VDNIEAPH',
         u'language': u'',
         u'libraryCatalog': u'library.catalogue.tcd.ie Library Catalog',
         u'numPages': u'',
         u'numberOfVolumes': u'',
         u'place': u'Evanston, Ill',
         u'publisher': u'Northwestern University Press',
         u'relations': {u'dc:replaces': u'http://zotero.org/users/436/items/9TXN8QUD'},
         u'rights': u'',
         u'series': u'',
         u'seriesNumber': u'',
         u'shortTitle': u'Saying I No More',
         u'tags': [{u'tag': u'Beckett, Samuel', u'type': 1},
                   {u'tag': u'Consciousness in literature', u'type': 1},
                   {u'tag': u'English prose literature', u'type': 1},
                   {u'tag': u'Ireland', u'type': 1},
                   {u'tag': u'Irish authors', u'type': 1},
                   {u'tag': u'Modernism (Literature)', u'type': 1},
                   {u'tag': u'Prose', u'type': 1},
                   {u'tag': u'Self in literature', u'type': 1},
                   {u'tag': u'Subjectivity in literature', u'type': 1}],
         u'title': u'Saying I No More: Subjectivity and Consciousness in The Prose of Samuel Beckett',
         u'url': u'',
         u'version': 792,
         u'volume': u''}

config = configparser.ConfigParser()
config.read('config.ini')
EMAIL = config['pubmed']['email']
QUERY = config['pubmed']['query'].replace('\n',' ')
QUERY_REVIEWS = config['pubmed']['query']

class Pubmed():
    def __init__(self, email):
        self.entrez = Entrez
        self.entrez.email = email
        self.db = "pubmed"
        self.articles_id_list = None
        self.articles_raw = None
        self.articles_formated = None
    def search(self, query):
        handle = self.entrez.esearch(db=self.db, term=query, retmax="40")
        self.articles_id_list =Entrez.read(handle)['IdList'] 
    def get_articles(self):
        response = self.entrez.esummary(db=self.db, id=','.join(self.articles_id_list)) 
        self.articles_raw = Entrez.read(response)


    def format_article_summary(self, raw_article):
        print('---------------')
        print(raw_article.keys())
        for k,v in raw_article.items():
            print(k,v)
        print('---------------')
    def format_articles(self):
        self.articles_formated = list(map(self.format_article_summary, self.articles_raw))


if __name__ == "__main__":
    p=Pubmed(EMAIL)
    p.search(QUERY)
    p.get_articles()
    p.format_articles()
    print(p.articles_id_list)
    print('---------------\n')
    print(p.articles_raw)
    print(p.articles_formated)


